FROM nginx:1.15.5-alpine

LABEL name=docker-nginx \
      version=1.0.2 \
      maintainer="dev@florianrusch.de"

COPY ./conf.d/nginx.conf /etc/nginx/conf.d/default.conf
COPY ./conf.d/ssl.conf /etc/nginx/conf.d/ssl.conf
COPY docker-entrypoint.sh /usr/local/bin/

# Updating the system
RUN apk update && \
    apk upgrade && \
    apk add bash openssl && \
    rm -rf /var/cache/apk/* /tmp/* /usr/share/man && \
    mkdir -p /etc/nginx/ssl

ENTRYPOINT ["docker-entrypoint.sh"]

CMD ["nginx", "-g", "daemon off;"]
