#!/bin/bash

echo ">> show current envs"
env | sort
echo ""

DH_SIZE=${DH_SIZE:=2048}
DH="/etc/nginx/ssl/dh.pem"

if [ ! -e "$DH" ]
then
  echo ">> seems like the first start of nginx"
  echo ">> generating $DH with size: $DH_SIZE"
  openssl dhparam -out "$DH" $DH_SIZE
fi

if [ ! -e "/etc/nginx/ssl/cert.crt" ] || [ ! -e "/etc/nginx/ssl/key.key" ]
then
  echo ">> generating self signed cert in /etc/nginx/ssl"
  openssl req -x509 -newkey rsa:4086 \
  -subj "/C=XX/ST=XXXX/L=XXXX/O=XXXX/CN=localhost" \
  -keyout "/etc/nginx/ssl/key.key" \
  -out "/etc/nginx/ssl/cert.crt" \
  -days 3650 -nodes -sha256
fi

# exec CMD
echo ">> exec docker CMD"
echo "$@"
exec "$@"
